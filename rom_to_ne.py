import os
import string
import re

import hunspell

from rom_to_ne_own_dic import dic


class NepaliConversion:
    
    def __init__(self, sentence):
        self.sentence = sentence.lower()

        self.checker_ne = hunspell.HunSpell('nlp-tools/dict/ne_NP.dic', 'nlp-tools/dict/ne_NP.aff')
        self.checker_en = hunspell.HunSpell('nlp-tools/dict/en_US.dic', 'nlp-tools/dict/en_US.aff')

        with open('word_list.txt', 'r+') as f:
            self.com_both = set(f.read().split())
        with open('word_list_2.txt', 'r+') as f:
            self.com_2 = set(f.read().split())
        with open('word_list_rem.txt', 'r+') as f:
            self.com_rem = set(f.read().split())

    
    def finalize_sentence(self):
        word_store = self.find_valid_word_from_dic(self.sentence)
        word_store = self.remove_space(word_store)
        final_sen = self.final_sentence(word_store) 

        for i in range(len(final_sen)):
            print(final_sen[i])     
    
    def ro_to_ne_converter(self, word):
        return_word = []
        l = len(word)
        sen = []
        count = 0
        for i in range(l):
            i = count
            for j in range(l, 0, -1):
                chunk1 = word[i:j]
                l_chunk1 = len(chunk1)
                if chunk1 in dic.keys():
                    count += l_chunk1
                    chunk2 = dic[chunk1]
                    sen.append(chunk2)
                    break   
        if len(sen):
            list_0 = sen[0]
            l_0 = len(list_0)
            l_all = len(sen)
            for i in range(1, l_all):
                temp_list = []
                for char_app in sen[i]:
                    new_list = [char + char_app for char in list_0]
                    temp_list.extend(new_list)
                list_0 = temp_list
            return_word = list_0

        return return_word
    
    def find_valid_word_from_dic(self, sentence):
        word_store = []
        sentence = sentence.strip().split(" ")
        
        for i, char in enumerate(sentence):
            word_store.append([])
            if char in self.com_both:
                if char in self.com_2:
                    word_store[i].append(char)
                elif char in self.com_rem:
                    word_store[i].append(char)
                    word_list = self.ro_to_ne_converter(char)
                    if word_list:
                        for word in word_list:
                            if self.checker_ne.spell(word):
                                checker = self.checker_ne.spell(word)
                                word_store[i].append(word)
                else:
                    word_list = self.ro_to_ne_converter(char)
                    if word_list:
                        for word in word_list:
                            if self.checker_ne.spell(word):
                                checker = self.checker_ne.spell(word)
                                word_store[i].append(word)
            else:
                if self.checker_en.spell(char):
                        checker = self.checker_en.spell(char)
                        word_store[i].append(char)
                else:
                    test_count = 0
                    word_list = self.ro_to_ne_converter(char)
                    if word_list:
                        for word in word_list:
                            if self.checker_ne.spell(word):
                                checker = self.checker_ne.spell(word)
                                word_store[i].append(word)
                                test_count = 1
                    if test_count == 0:
                        word_store[i].append(char)          

        return word_store
    
    def remove_space(self, word_store):
        if len(word_store) > 1:
            for i in range(len(word_store)):
                if [] in word_store:
                    word_store.remove([])
        return word_store
    
    def final_sentence(self, word_store):
        if len(word_store):
            word_0 = word_store[0]
            l_0 = len(word_0)
            l_all = len(word_store)
            for i in range(1, l_all):
                temp_list = []
                for char_app in word_store[i]:
                    new_list = [char + ' ' + char_app for char in word_0]
                    temp_list.extend(new_list)
                word_0 = temp_list
        return word_0

    
def main():
    sentence = input("Enter a sentence to be romanized:")
    rom_to_ne = NepaliConversion(sentence)
    rom_to_ne.finalize_sentence()


if '__main__' == __name__:
    main()
    